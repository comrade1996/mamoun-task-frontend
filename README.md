# Mamoun Checkout Task


## Development server

Run `npm install` for install dependencies.

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Live Demo
visit `https://mamoun-omir-task.netlify.app/` for live demo.

## Notes
The backend has been mocked in this Project.

## TODOS
add websocket
add e2e test
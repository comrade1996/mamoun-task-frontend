import {Component, ElementRef, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CheckoutService} from "./data/checkout.service";
import {customValidators} from "./utils/common/customValidators/customValidators.Validator";
import {Pay} from "./Models/Pay.interface";
import {NotificationService} from "./utils/common/services/notification.service";
import {CustomSpinnerComponent} from "./utils/common/components/custom-spinner/custom-spinner.component";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mamoun-checkout';

    checkoutForm: FormGroup ;

    creditCardNameRegex="^[a-zA-Z- ]*$";

    creditCardExpiryDateRegex="([0-9]{2}[/]?){2}";

    creditCardPriceRegex="^[0-9-.]*$";


    submitted=false;

    public customSpinnerComponent= CustomSpinnerComponent;

    payed= false;

    successResponse:any;

    constructor(private fb: FormBuilder,private checkoutAPI:CheckoutService,private notification :NotificationService) {}

    ngOnInit() {
        this.initCheckoutForm();
    }


initCheckoutForm(){
    this.checkoutForm = this.fb.group({
        cardName: ['', [Validators.required,Validators.maxLength(100),Validators.pattern(this.creditCardNameRegex) ]],
        cardNumber: ['', [Validators.required,customValidators.equalLength(19)]],
        ExpiryDate: ['', [Validators.required,Validators.pattern(this.creditCardExpiryDateRegex)]],
        cvc: ['', [Validators.required,customValidators.equalLength(3)]],
        price: [0, [Validators.required,Validators.pattern(this.creditCardPriceRegex),Validators.min(0.01),Validators.max(100000)]],
        currency: ['USD', [Validators.required]],
        saved: [false, [Validators.required]],
    });
}
    creditCardNumberSpacing(){
        console.log(this.f['cardNumber'].value.toString())
        const input = this.f['cardNumber'];
        let v = input.value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
        let matches = v.match(/\d{4,16}/g);
        let match = matches && matches[0] || ''
        let parts = []

        for (let i=0, len=match.length; i<len; i+=4) {
            parts.push(match.substring(i, i+4))
        }

        if (parts.length) {
            this.f['cardNumber'].setValue( parts.join(' '));
        }
        console.log(this.f['cardNumber'].value.toString().length)
    }

     CurrencyChange(){
         console.log("from price")
        this.checkoutAPI.price({requiredCurrency:this.f['currency'].value,amount:this.f['price'].value}).subscribe(data=>this.f['price'].setValue(data))
     }
get f(){
        return this.checkoutForm.controls;
}

    onSubmit(form: FormGroup) {
        console.log({form})
        //TODO Global handling for notifications

        this.submitted= true;
      this.checkoutAPI.pay(form.value as Pay).subscribe(data => {
          this.submitted=false;
          this.payed=true;
          this.successResponse=data;
          console.log({data})
          this.notification.showSuccess('Payment done successfully','Success');
this.reset();
      },error => {
          this.submitted=false;

          console.log(error)
          this.notification.showError(error.error.message,"Oppss !!");

      }) ;
    }

    private reset() {
        this.checkoutForm.reset();
    }
}

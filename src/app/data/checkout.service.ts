import { Injectable } from '@angular/core';
import {httpApiService} from "../utils/common/services/httpApiService.service";
import {PriceConverter} from "../Models/PriceConverter.interface";
import {Pay} from "../Models/Pay.interface";

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(private api:httpApiService) { }

    price(priceCon:PriceConverter){
        return this.api.post('price',priceCon);
    }

    pay(payData:Pay){
      if (payData.saved)
          { // @ts-ignore
              localStorage.setItem("userDetail",JSON.stringify(payData));
          }
      //TODO refactor to send full data
        return this.api.post('pay',payData);
    }


}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ReactiveFormsModule} from "@angular/forms";
import {fakeBackendProvider} from "./utils/backendMocker/FakeBackendInterceptor.Interceptor";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {GlobalHttpErrorInterceptorService} from "./utils/common/errors/GlobalHttpInterceptor";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ToastrModule} from "ngx-toastr";
import {NgHttpLoaderModule} from "ng-http-loader";
import {SweetAlert2Module} from "@sweetalert2/ngx-sweetalert2";
import { CustomSpinnerComponent } from './utils/common/components/custom-spinner/custom-spinner.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
      HttpClientModule,
      ReactiveFormsModule,
      SweetAlert2Module.forRoot(),
      NgHttpLoaderModule.forRoot(),
      BrowserAnimationsModule,
      ToastrModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
      fakeBackendProvider,
  ],
  bootstrap: [AppComponent],
    entryComponents:[
        CustomSpinnerComponent
    ]
})
export class AppModule { }

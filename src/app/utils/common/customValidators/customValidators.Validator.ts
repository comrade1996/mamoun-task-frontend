import {AbstractControl, ValidatorFn} from '@angular/forms';

export class customValidators {


    static equalLength(validLength: number): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null =>
            control.value?.toString().length === validLength
                ? null : {notEqual: control.value};
    }
}
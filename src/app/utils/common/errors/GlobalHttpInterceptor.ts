import {NotificationService} from "../services/notification.service";

import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor,HttpRequest} from '@angular/common/http';
import {Observable, throwError} from "rxjs";
import {catchError} from 'rxjs/operators';

@Injectable()
export class GlobalHttpErrorInterceptorService implements HttpInterceptor {

    constructor(private notification:NotificationService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(req).pipe(
            catchError((error) => {
                console.log()
                console.log('error is intercept')
                console.error(error);

                this.notification.showError(error.message,"oppss!")//Refactor TODO
                return throwError(error.message);
            })
        )
    }
}
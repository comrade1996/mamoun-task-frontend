import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class httpApiService {
    private baseUrl = environment.url;


    constructor(
        private http: HttpClient,
    ) {}

    public post(url: string, data: any, options?: any){
        return this.http.post( this.appendBaseUrl(url), data,options );
    }
    private appendBaseUrl(shortUrl: string) {
        return `${this.baseUrl}/${shortUrl}`;
    }
}

import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import {PriceConverter} from "../../Models/PriceConverter.interface";
import {Pay} from "../../Models/Pay.interface";
import {NotificationService} from "../common/services/notification.service";

// array in local storage for registered users
let users = JSON.parse(<string>localStorage.getItem('users')) || [];

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    constructor(private notification:NotificationService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const { url, method, headers, body } = request;


        // wrap in delayed observable to simulate server api call
        return of(null)
            .pipe(mergeMap(handleRoute))
            .pipe(materialize()) // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .pipe(delay(500))
            .pipe(dematerialize());

        function handleRoute() {
            switch (true) {

                case url.includes('price') && method === 'POST': {
                    return price(body);
                }
                case url.includes('pay') && method === 'POST':
                    return pay(body);
                default:
                    // pass through any requests not handled above
                    return next.handle(request);
            }
        }

        // route functions


        function pay(body: Pay) {
            //TODO Refactor to handle bigInt
            if (body.saved)
            localStorage.setItem('payDetail', JSON.stringify(body));

            if (body.cardNumber.toString().slice(-1) != '5')
            return ok('success');

             return throwError({ status: 404, error: { message: 'Card Not found' } });
        }

        function price(body: PriceConverter) {
            //TODO refactor to Decorator Pattern
            console.log(body.amount * 3.9)
            console.log(body.amount / 3.9)
            if (body.requiredCurrency=="USD")
                return  ok((body.amount * 3.9).toFixed(0));

            if (body.requiredCurrency=="OMR")
                return  ok((body.amount / 3.9).toFixed(2));


                return  ok((body.amount));
            // else
            //    return ok(((body.amount / 3.9 ) + (body.amount % 3.9)).toFixed(2));

            //    return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }

        // helper functions

        function ok(body?:any) {
            return of(new HttpResponse({ status: 200, body }))
        }

        function error(message:any) {
            return throwError({ error: { message } });
        }




    }
}

export const fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
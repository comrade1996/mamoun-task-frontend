export interface Socket {
    // @ts-ignore
    on(event: string, callback: (data: any) => void );

    // @ts-ignore
    emit(event: string, data: any);
}
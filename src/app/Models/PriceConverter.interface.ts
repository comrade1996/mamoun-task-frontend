export interface PriceConverter{
    requiredCurrency:string,
    amount:number
}
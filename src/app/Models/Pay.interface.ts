export interface Pay {
    cardName:   string;
    cardNumber: number;
    ExpiryDate: string;
    cvc:        string;
    price:      string;
    currency:   string;
    saved:      boolean;
}
